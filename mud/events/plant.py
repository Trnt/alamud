from .event import Event2


class PlantEvent(Event2):
    NAME = "plant"

    def perform(self):
        self.object.move_to(self.actor.container())
        self.inform("plant")