from .action import Action2
from mud.events import PlantEvent

class PlantAction(Action2):
    EVENT = PlantEvent
    RESOLVE_OBJECT = "resolve_for_use"
    ACTION = "plant"